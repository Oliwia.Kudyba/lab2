package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    int maxFridge = 20;
    ArrayList<FridgeItem> fridgeitems = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return fridgeitems.size();
    }

    @Override
    public int totalSize() {
        return maxFridge;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < 20){
            fridgeitems.add(item);
            return true;
        }
        if (nItemsInFridge() == totalSize()){
            return false;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeitems.contains(item)){
            fridgeitems.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeitems.clear();
        }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for(FridgeItem items : fridgeitems){
            if (items.hasExpired()){
                expiredItems.add(items);
            }
        }
        for (FridgeItem expired : expiredItems){
            fridgeitems.remove(expired);
        }
        return expiredItems;
    }
}
